//    totem-py-parser python library wrapper around totem-pl-parser.
//    Copyright (C) 2009  Alexey Kuznetsov <ak@axet.ru>
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <pygobject.h>

extern PyMethodDef totem_pl_parser_functions[];

void totem_pl_parser_add_constants(PyObject *module, const gchar *strip_prefix);
void totem_pl_parser_register_classes(PyObject *d);

DL_EXPORT(void) inittotem_py_parser(void)
{
  PyObject *m, *d;

  init_pygobject();

  m = Py_InitModule("totem_py_parser", totem_pl_parser_functions);
  d = PyModule_GetDict(m);

  totem_pl_parser_register_classes(d);
  if(PyErr_Occurred()){
    Py_FatalError ("can't initialise module totem_pl_parser");
  }

  totem_pl_parser_add_constants(m, "");
}
